import babel from '@rollup/plugin-babel';
import ts from '@rollup/plugin-typescript';
import commonjs from '@rollup/plugin-commonjs';

export default {
  input: ['../bcoe-v8-coverage/src/lib/index.ts'],
  output: {
    dir: '../bcoe-v8-coverage/dist/lib',
    format: 'cjs',
  },
  plugins: [
    babel({ babelHelpers: 'bundled' }),
    commonjs(),
    ts({
      declaration: true,
      skipLibCheck: true,
      exclude:["node_modules","debian","test","gulpfile.ts"],
      allowSyntheticDefaultImports: true,
      downlevelIteration: true,
      declarationDir:'../bcoe-v8-coverage/dist/lib',
    })
  ]
};
