# Installation
> `npm install --save @types/vinyl-fs`

# Summary
This package contains type definitions for vinyl-fs ( https://github.com/gulpjs/vinyl-fs ).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/vinyl-fs

Additional Details
 * Last updated: Thu, 14 Feb 2019 00:21:08 GMT
 * Dependencies: @types/vinyl, @types/glob-stream, @types/node
 * Global values: none

# Credits
These definitions were written by vvakame <https://github.com/vvakame>, remisery <https://github.com/remisery>, TeamworkGuy2 <https://github.com/TeamworkGuy2>.
