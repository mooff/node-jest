# Installation
> `npm install --save @types/weak-napi`

# Summary
This package contains type definitions for weak-napi (https://github.com/node-ffi-napi/weak-napi).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/weak-napi.

### Additional Details
 * Last updated: Sat, 08 Aug 2020 03:50:32 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Hieu Lam](https://github.com/lamhieu-vk).
