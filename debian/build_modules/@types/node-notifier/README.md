# Installation
> `npm install --save @types/node-notifier`

# Summary
This package contains type definitions for node-notifier (https://github.com/mikaelbr/node-notifier).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/node-notifier.

### Additional Details
 * Last updated: Wed, 16 Sep 2020 17:27:16 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Qubo](https://github.com/tkQubo), [Lorenzo Rapetti](https://github.com/loryman), and [Piotr Błażejewicz](https://github.com/peterblazejewicz).
