# Installation
> `npm install --save @types/gulp`

# Summary
This package contains type definitions for Gulp (http://gulpjs.com).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/gulp.

### Additional Details
 * Last updated: Tue, 29 Sep 2020 02:44:33 GMT
 * Dependencies: [@types/vinyl-fs](https://npmjs.com/package/@types/vinyl-fs), [@types/chokidar](https://npmjs.com/package/@types/chokidar), [@types/undertaker](https://npmjs.com/package/@types/undertaker)
 * Global values: none

# Credits
These definitions were written by [Drew Noakes](https://drewnoakes.com), [Juan Arroyave](http://jarroyave.co), and [Giedrius Grabauskas](https://github.com/GiedriusGrabauskas).
