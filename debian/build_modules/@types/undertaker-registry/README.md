# Installation
> `npm install --save @types/undertaker-registry`

# Summary
This package contains type definitions for undertaker-registry (https://github.com/gulpjs/undertaker-registry).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/undertaker-registry

Additional Details
 * Last updated: Wed, 25 Oct 2017 16:19:00 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Giedrius Grabauskas <https://github.com/GiedriusGrabauskas>.
