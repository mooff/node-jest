# Installation
> `npm install --save @types/undertaker`

# Summary
This package contains type definitions for undertaker (https://github.com/gulpjs/undertaker).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/undertaker.

### Additional Details
 * Last updated: Mon, 04 Jan 2021 20:50:48 GMT
 * Dependencies: [@types/undertaker-registry](https://npmjs.com/package/@types/undertaker-registry), [@types/async-done](https://npmjs.com/package/@types/async-done), [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Qubo](https://github.com/tkqubo), [Giedrius Grabauskas](https://github.com/GiedriusGrabauskas), and [Evan Yamanishi](https://github.com/sh0ji).
